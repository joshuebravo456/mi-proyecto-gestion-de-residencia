  // Obtiene una referencia al botón
  const botonMostrarImagen = document.getElementById("botonMostrarImagen");

  // Agrega un evento click al botón
  botonMostrarImagen.addEventListener("click", function() {
    // Llama a la función mostrarImagenCompleta cuando se hace clic en el botón
    mostrarImagenCompleta(imagen); // Asegúrate de tener la variable "imagen" definida antes de este punto
  });

  function mostrarImagenCompleta(imagen) {
    const imagenCompleta = document.getElementById("imagenCompleta");
    const imagenCompletaImg = imagenCompleta.querySelector("img");

    // Asigna la imagen en el contenedor de la imagen completa
    imagenCompletaImg.src = imagen.src;

    // Muestra el contenedor de la imagen completa
    imagenCompleta.style.display = "block";

    // Agrega la clase al body para ocultar el scroll
    document.body.classList.add("overflow-hidden");
  }


function cerrarImagenCompleta() {
const imagenCompleta = document.getElementById("imagenCompleta");

// Oculta el contenedor de la imagen completa
imagenCompleta.style.display = "none";

// Remueve la clase para mostrar el scroll
document.body.classList.remove("overflow-hidden");
}