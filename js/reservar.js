document.addEventListener("DOMContentLoaded", function () {
  // Seleccionar elementos del formulario y métodos de pago
  const metodoPago = document.getElementById('metodo-pago');
  const tarjeta = document.getElementById('tarjeta');
  const transferencia = document.getElementById('transferencia');
  const paypal = document.getElementById('paypal');

  // Ocultar campos adicionales al cargar la página
  tarjeta.classList.add('hidden');
  transferencia.classList.add('hidden');
  paypal.classList.add('hidden');

  // Mostrar campos adicionales según el método de pago seleccionado
  metodoPago.addEventListener('change', function () {
    // Ocultar todos los campos adicionales primero
    tarjeta.classList.add('hidden');
    transferencia.classList.add('hidden');
    paypal.classList.add('hidden');

    // Luego mostrar el campo adicional correspondiente a la opción seleccionada
    if (metodoPago.value === 'tarjeta') {
      tarjeta.classList.remove('hidden');
    } else if (metodoPago.value === 'transferencia') {
      transferencia.classList.remove('hidden');
    } else if (metodoPago.value === 'paypal') {
      paypal.classList.remove('hidden');
    }
  });

  // Manejar el evento de envío del formulario
  document.getElementById('reserva-form').addEventListener('submit', function (event) {
    // Validación de la fecha
    const fechaEntrada = new Date(document.getElementById('fecha-entrada').value);
    const fechaSalida = new Date(document.getElementById('fecha-salida').value);

    if (fechaEntrada >= fechaSalida) {
      alert('La fecha de entrada debe ser anterior a la fecha de salida.');
      event.preventDefault();
      return;
    }

    // Validación del nombre
    const nombre = document.getElementById('nombre').value;
    if (nombre.trim() === '') {
      alert('Ingresa un nombre válido.');
      event.preventDefault();
      return;
    }

    // Validación del correo
    const correo = document.getElementById('correo').value;
    const correoRegExp = /^[a-zA-Z0-9._-]+@[a-zAZ0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (!correoRegExp.test(correo)) {
      alert('Ingresa un correo electrónico válido.');
      event.preventDefault();
      return;
    }

    // Validación del teléfono
    const telefono = document.getElementById('telefono').value;
    if (isNaN(telefono) || telefono.length < 9) {
      alert('Ingresa un número de teléfono válido (al menos 9 dígitos).');
      event.preventDefault();
      return;
    }

    // Validación del método de pago seleccionado
    if (metodoPago.value === 'tarjeta') {
      const numeroTarjeta = document.getElementById('numero-tarjeta').value;
      const nombreTarjeta = document.getElementById('nombre-tarjeta').value;
      const fechaVencimiento = document.getElementById('fecha-vencimiento').value;
      const codigoSeguridad = document.getElementById('codigo-seguridad').value;

      if (/^\d{16}$/.test(numeroTarjeta.replace(/\s/g, ' ')) || nombreTarjeta.trim() === '' || fechaVencimiento === '' || !/^\d{3}$/.test(codigoSeguridad)) {
        alert('Ingresa información válida para el método de pago con tarjeta.');
        event.preventDefault();
        return;
      }
    } else if (metodoPago.value === 'transferencia') {
      const banco = document.getElementById('banco').value;
      const cuenta = document.getElementById('cuenta').value;

      if (banco.trim() === '' || cuenta.trim() === '') {
        alert('Ingresa información válida para el método de pago por transferencia.');
        event.preventDefault();
        return;
      }
    } else if (metodoPago.value === 'paypal') {
      const correoPaypal = document.getElementById('correo-paypal').value;
      const correoPaypalRegExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

      if (!correoPaypalRegExp.test(correoPaypal)) {
        alert('Ingresa un correo de PayPal válido.');
        event.preventDefault();
        return;
      }
    }

    // Si todo está válido, el formulario se enviará
    alert('¡Reserva exitosa!');
  });
});
