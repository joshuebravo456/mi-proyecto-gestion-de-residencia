// Obtención de la lista de usuarios o inicialización si es la primera vez
const userList = JSON.parse(sessionStorage.getItem("userList")) || [];

// Verificar si el usuario administrador ya existe en la lista
const adminExists = userList.some(user => user.rol === 'Administrador');

// Agregar un usuario administrador por defecto si no existe en la lista
if (!adminExists) {
    const usuarioAdministrador = {
        username: "admin",
        email: "admin@example.com",
        telefono: '0',
        password: "admin123",
        habitacion: '0',
        rol: 'Administrador'
    };
    userList.push(usuarioAdministrador);
    sessionStorage.setItem("userList", JSON.stringify(userList));
}

// Obtención del formulario de inicio de sesión por su ID
const loginForm = document.getElementById("login-form");

// Agregar un manejador de eventos al formulario cuando se envía
loginForm.addEventListener("submit", function (event) {
    // Prevenir el comportamiento predeterminado del formulario (evitar que se envíe)
    event.preventDefault();

    // Obtener los valores ingresados en los campos de nombre de usuario y contraseña
    const usernameInput = document.getElementById("username").value;
    const passwordInput = document.getElementById("password").value;

    // Realizar la autenticación basada en el rol de usuario
    const user = userList.find((user) => user.username === usernameInput && user.password === passwordInput);

    if (user) {
        if (user.rol === "Administrador") {
            // Si el usuario tiene el rol de administrador, redirigir a la página de administrador
            alert("¡Sesión iniciada correctamente como administrador!");
            window.location.href = "administrador.html";
        } else {
            // Si el usuario no tiene el rol de administrador, mostrar un mensaje de acceso denegado
            alert("Acceso denegado. No tienes permiso para acceder a esta página.");
        }
    } else {
        // Si no se encuentra el usuario en la lista, mostrar un mensaje de credenciales incorrectas
        alert("Credenciales incorrectas. Por favor, intenta de nuevo.");
    }
});
