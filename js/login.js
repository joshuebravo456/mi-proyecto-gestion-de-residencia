function iniciarSesion(event) {
    event.preventDefault();
  
    // Obtener los valores ingresados en los campos de entrada
    const usernameInput = document.querySelector('#login-username');
    const passwordInput = document.querySelector('#login-password');
  
    // Obtener la lista de usuarios registrados desde sessionStorage
    const userList = JSON.parse(sessionStorage.getItem("userList")) || [];
  
    if (userList.length === 0) {
      alert("No hay usuarios registrados. Regístrese primero.");
      return false;
    }
  
    // Realizar la búsqueda en la lista de usuarios registrados
    const foundUser = userList.find(user => user.username === usernameInput.value && user.password === passwordInput.value);
  
    if (foundUser) {
      alert("¡Sesión iniciada correctamente!");
      window.location.href = "habitacion.html"; // Redirige a la página de habitación si las credenciales son correctas
    } else {
      alert("Credenciales incorrectas. Por favor, inténtelo de nuevo.");
    }
  }
  