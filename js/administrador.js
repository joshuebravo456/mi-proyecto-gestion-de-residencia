// Este código se ejecuta cuando el evento "DOMContentLoaded" ocurre, es decir, cuando la página se carga por completo.
document.addEventListener("DOMContentLoaded", function () {
  // Selecciona elementos del DOM y los almacena en variables.
  const tabla = document.getElementById("tabla-habitaciones");
  const tbody = tabla.querySelector("tbody");
  const formulario = document.getElementById("formulario");
  const guardarBtn = document.getElementById("guardar-btn");
  const agregarBtn = document.getElementById("agregar-btn");

  // Variable para rastrear la habitación en edición.
  let habitacionEditando = null;

  // Recupera datos del almacenamiento local (localStorage) al cargar la página.
  const habitacionesGuardadas = JSON.parse(localStorage.getItem("habitaciones")) || [];

  // Función para agregar una fila a la tabla.
  function agregarFila(numHabitaciones, tipo, capacidad, precio) {
      // Crea una fila (row) en la tabla y establece su contenido.
      const fila = document.createElement("tr");
      fila.innerHTML = `
          <td>${numHabitaciones}</td>
          <td>${tipo}</td>
          <td>${capacidad}</td>
          <td>${precio}</td>
          <td>
              <button class="editar-fila" class="btn">Editar</button>
              <button class="borrar-fila" class="btn">Borrar</button>
          </td>
      `;

      // Agrega eventos para editar y borrar filas.
      const editarBtn = fila.querySelector(".editar-fila");
      const borrarBtn = fila.querySelector(".borrar-fila");

      editarBtn.addEventListener("click", function () {
          // Muestra el formulario de edición con los datos de la habitación seleccionada.
          mostrarFormulario(numHabitaciones, tipo, capacidad, precio);
          habitacionEditando = numHabitaciones;
      });

      borrarBtn.addEventListener("click", function () {
          // Elimina la fila de la tabla y los datos guardados en localStorage.
          fila.remove();
          habitacionesGuardadas.splice(
              habitacionesGuardadas.findIndex((habitacion) => habitacion.numHabitaciones === numHabitaciones),
              1
          );
          localStorage.setItem("habitaciones", JSON.stringify(habitacionesGuardadas));
      });

      // Agrega la fila a la tabla.
      tbody.appendChild(fila);
  }

  // Función para mostrar el formulario de edición/agregación.
  function mostrarFormulario(tipo, capacidad, precio) {
      // Llena el formulario con los datos de la habitación seleccionada.
      document.getElementById("tipo").value = tipo;
      document.getElementById("capacidad").value = capacidad;
      document.getElementById("precio").value = precio;
      formulario.style.display = "block";
      agregarBtn.style.display = "none";
  }

  // Evento para guardar los datos en localStorage al hacer clic en el botón "Guardar".
  guardarBtn.addEventListener("click", function () {
      const tipo = document.getElementById("tipo").value;
      const capacidad = document.getElementById("capacidad").value;
      const precio = document.getElementById("precio").value;

      if (habitacionEditando) {
          // Actualiza los datos de la habitación editada.
          const index = habitacionesGuardadas.findIndex((habitacion) => habitacion.numHabitaciones === habitacionEditando);
          habitacionesGuardadas[index] = { numHabitaciones: habitacionEditando, tipo, capacidad, precio };
          habitacionEditando = null;
      } else {
          // Genera el número de habitaciones automáticamente.
          const numHabitaciones = 103 + habitacionesGuardadas.length + 1;

          // Agrega una nueva fila y guarda los datos en localStorage.
          agregarFila(numHabitaciones, tipo, capacidad, precio);
          habitacionesGuardadas.push({ numHabitaciones, tipo, capacidad, precio });
      }

      localStorage.setItem("habitaciones", JSON.stringify(habitacionesGuardadas));

      // Limpia el formulario y lo oculta.
      formulario.style.display = "none";
      agregarBtn.style.display = "block";
  });

  // Evento para mostrar el formulario al hacer clic en el botón "Agregar Habitación".
  agregarBtn.addEventListener("click", function () {
      mostrarFormulario("", "", "");
  });

  // Agrega filas a la tabla a partir de los datos guardados en localStorage.
  habitacionesGuardadas.forEach((habitacion) => {
      agregarFila(habitacion.numHabitaciones, habitacion.tipo, habitacion.capacidad, habitacion.precio);
  });
});


// La siguiente parte del código parece ser otra funcionalidad para la gestión de usuarios.
const formulario = document.querySelector('#registro-estudiante');
const tablaestudiante = document.querySelector('.tabla-estudiantes tbody');

// Variable para almacenar la lista de usuarios en la sesión o crear una lista vacía si no existe.
let userList = JSON.parse(sessionStorage.getItem("userList")) || [];
let filaEditando = null;

// Función para cargar los datos del usuario en el formulario de edición.
function cargarDatosEnFormulario(usuario) {
document.querySelector('#nombre').value = usuario.nombre;
document.querySelector('#email').value = usuario.email;
document.querySelector('#telefono').value = usuario.telefono;
document.querySelector('#tipo-habitacion').value = usuario.habitacion;
document.querySelector('#rol').value = usuario.rol;
document.querySelector('#contrasena').value = usuario.contrasena;
}

// Función para cancelar la edición.
function cancelarEdicion() {
filaEditando = null;
formulario.reset();
}

// Función para guardar la edición de un usuario.
function guardarEdicion() {
if (filaEditando) {
  const index = userList.indexOf(filaEditando.usuario);
  if (index !== -1) {
    userList[index] = {
      nombre: document.querySelector('#nombre').value,
      email: document.querySelector('#email').value,
      telefono: document.querySelector('#telefono').value,
      habitacion: document.querySelector('#tipo-habitacion').value,
      rol: document.querySelector('#rol').value,
      contrasena: document.querySelector('#contrasena').value
    };

    const fila = filaEditando.fila;
    fila.children[0].textContent = userList[index].nombre;
    fila.children[1].textContent = userList[index].email;
    fila.children[2].textContent = userList[index].telefono;
    fila.children[3].textContent = userList[index].habitacion;
    fila.children[4].textContent = userList[index].rol;
    fila.children[5].textContent = userList[index].contrasena;

    sessionStorage.setItem("userList", JSON.stringify(userList));
  }

  filaEditando = null;
  formulario.reset();
}
}

// Función para crear una fila de usuario en la tabla.
function crearFilaUsuario(usuario) {
const fila = document.createElement('tr');
fila.innerHTML = `
  <td>${usuario.username}</td>
  <td>${usuario.email}</td>
  <td>${usuario.telefono}</td>
  <td>${usuario.habitacion}</td>
  <td>${usuario.rol}</td>
  <td>${usuario.password}</td>
  <td>
    <button type="button" class="editar-fila" class="btn">Editar</button>
    <button type="button" class="eliminar-fila" class="btn">Eliminar</button>
  </td>
`;

tablaestudiante.appendChild(fila);

fila.querySelector('.editar-fila').addEventListener('click', () => {
  filaEditando = { usuario, fila };
  cargarDatosEnFormulario(usuario);
});

fila.querySelector('.eliminar-fila').addEventListener('click', () => {
  if (fila.rowIndex <= 2) {
    alert('No se puede eliminar esta fila.');
  } else {
    fila.remove();
    const index = userList.indexOf(usuario);
    if (index !== -1) {
      userList.splice(index, 1);
      sessionStorage.setItem("userList", JSON.stringify(userList));
    }
  }
});
}

// Itera a través de la lista de usuarios y crea filas para cada uno.
userList.forEach((usuario) => {
crearFilaUsuario(usuario);
});

// Evento para manejar el envío del formulario.
formulario.addEventListener('submit', (event) => {
event.preventDefault();

if (filaEditando) {
  guardarEdicion();
} else {
  const nombre = document.querySelector('#nombre').value;
  const email = document.querySelector('#email').value;
  const telefono = document.querySelector('#telefono').value;
  const habitacion = document.querySelector('#tipo-habitacion').value;
  const rol = document.querySelector('#rol').value;
  const contrasena = document.querySelector('#contrasena').value;

  const usuario = {
    username,
    email,
    telefono,
    habitacion,
    rol,
    password,
  };

  userList.push(usuario);
  sessionStorage.setItem("userList", JSON.stringify(userList));
  crearFilaUsuario(usuario);
}

formulario.reset();
filaEditando = null;
});

// Función para mostrar el formulario de registro/agregación de usuario.
function mostrarFormulario() {
formulario.reset();
filaEditando = null;
formulario.style.display = 'block';
}

// Eventos para cancelar y guardar la edición de usuario.
document.querySelector('#cancelar-edicion').addEventListener('click', () => {
cancelarEdicion();
});

document.querySelector('#guardar-edicion').addEventListener('click', () => {
guardarEdicion();
});

