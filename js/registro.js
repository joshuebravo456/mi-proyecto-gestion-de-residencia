function registrar(event) {
    event.preventDefault();
  
    const usernameInput = document.querySelector('#username');
    const emailInput = document.querySelector('#email');
    const passwordInput = document.querySelector('#password');
    const confirmPasswordInput = document.querySelector('#confirm-password');
  
    // Expresión regular para validar el formato del correo electrónico
    const emailFormat = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
  
    if (usernameInput.value === "" || emailInput.value === "" || passwordInput.value === "" || confirmPasswordInput.value === "") {
      alert("Por favor, complete todos los campos.");
      return false;
    }
  
    if (!emailFormat.test(emailInput.value)) {
      alert("El correo electrónico no tiene un formato válido.");
      return false;
    }
  
    if (passwordInput.value !== confirmPasswordInput.value) {
      alert("Las contraseñas no coinciden.");
      return false;
    }
  
    // Obtener la lista actual de usuarios registrados o inicializarla si es la primera vez
    const userList = JSON.parse(sessionStorage.getItem("userList")) || [];
  
    // Agregar el nuevo usuario a la lista
    userList.push({
      username: usernameInput.value,
      email: emailInput.value,
      telefono: '0',
      password: passwordInput.value,
      habitacion: '0',
      rol: 'Estudiante'
    });
  
    // Almacenar la lista actualizada en sessionStorage
    sessionStorage.setItem("userList", JSON.stringify(userList));
  
    alert("¡Registrado correctamente!");
    window.location.href = "login.html"; // Redirige a la página de inicio de sesión con un registro exitoso
  }
  